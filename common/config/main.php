<?php
use cinghie\mailchimp\components\Mailchimp as MailchimpComponent;
use cinghie\mailchimp\Mailchimp;
return [
    'id' => 'advanced',
    'name' => 'User Management',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'modules' => [
        'rbac' => [
            'class' => 'yii2mod\rbac\Module',
        ],
        'user' => [
            'class' => 'dektrium\user\Module',
            'enableFlashMessages' => false,
            'enableRegistration' => true,
            'enableConfirmation' => true,
            'adminPermission' => 'admin',
            'modelMap' => [
                'RegistrationForm' => 'common\models\RegistrationForm',
            ],
            'controllerMap' => [
                'registration' => [
                    'class' => 'common\controllers\RegistrationController',
                ],
            ],
        ],
    ],
    'components' => [
        'mailchimp' => [
            'class' => MailchimpComponent::class,
            'apiKey' => 'a514b1d27150fe35b046763489deb11c-us7'
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'authManager' => [
            'class' => 'dektrium\rbac\components\DbManager',
        ],
        'i18n' => [
            'translations' => [
                '*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@app/messages'
                ],
            ],
        ],
        'view' => [
            'theme' => [
                'pathMap' => [
                    '@dektrium/user/views' => '@common/views/user',
                ],
            ],
        ],
    ],
];

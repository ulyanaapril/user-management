<?php

namespace common\models;

use dektrium\user\models\User;
use Yii;

/**
 * This is the model class for table "{{%invite}}".
 *
 * @property int $id
 * @property string|null $username
 * @property string|null $email
 * @property string|null $sent_date
 * @property string|null $registration_date
 * @property string|null $last_signin
 * @property string|null $sex
 * @property string|null $location
 * @property string|null $status
 */
class Invite extends \yii\db\ActiveRecord
{
    public static $STATUS_INVITED = 'invited';
    public static $STATUS_REGISTERED = 'registered';
    public static $STATUS_BLOCKED = 'blocked';
    public static $STATUS_REMOVED = 'removed';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%invite}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sent_date', 'registration_date', 'last_signin'], 'safe'],
            [['username', 'token'], 'string', 'max' => 255],
            [['username'], 'unique', 'targetClass' => 'dektrium\user\models\User'],
            [['email'], 'string', 'max' => 100],
            [['user_id'], 'integer'],
            [['email'], 'unique', 'targetClass' => 'dektrium\user\models\User'],
            [['sex', 'status'], 'string', 'max' => 50],
            [['location'], 'string', 'max' => 150],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'username' => Yii::t('app', 'Name'),
            'email' => Yii::t('app', 'Email'),
            'sent_date' => Yii::t('app', 'Sent Date'),
            'registration_date' => Yii::t('app', 'Registration Date'),
            'last_signin' => Yii::t('app', 'Last Signin'),
            'sex' => Yii::t('app', 'Sex'),
            'location' => Yii::t('app', 'Location'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @param $id
     * @return array|mixed|null
     */
    public function getStatus($id = null) {
        $statuses = [
            self::$STATUS_INVITED => Yii::t('app', 'Invited'),
            self::$STATUS_REGISTERED => Yii::t('app', 'Registered'),
            self::$STATUS_BLOCKED => Yii::t('app', 'Blocked'),
            self::$STATUS_REMOVED => Yii::t('app', 'Removed')
        ];
        if (!empty($id)) {
            if (in_array($id, $statuses)) {
                return $statuses[$id];
            } else {
                return null;
            }
        }
        return $statuses;
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}

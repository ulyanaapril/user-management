<?php
namespace common\models;

use dektrium\user\models\Profile as BaseProfile;
use Yii;


class Profile extends BaseProfile
{
    /**
     * @return array
     */
    public function rules()
    {
        $rules = parent::rules();
        $rules[] = [['sex'], 'string', 'max' => '50'];
        $rules[] = [['fullname'], 'string', 'max' => '150'];
        return $rules;
    }

}

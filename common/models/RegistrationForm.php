<?php
namespace common\models;

use dektrium\user\models\RegistrationForm as BaseForm;
use dektrium\user\models\User;
use Yii;


class RegistrationForm extends BaseForm
{
    public $fullname;
    public $sex;
    public $location;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = parent::rules();
        $rules[] = [['sex', 'fullname', 'location'], 'required'];
        $rules[] = [['sex'], 'string', 'max' => '50'];
        $rules[] = [['fullname'], 'string', 'max' => '150'];
        return $rules;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        $labels = parent::attributeLabels();
        $labels['fullname'] = \Yii::t('user', 'Name');
        $labels['sex'] = \Yii::t('user', 'Sex');
        $labels['location'] = \Yii::t('user', 'Location');
        return $labels;
    }

    /**
     * @inheritdoc
     */
    public function loadAttributes(User $user)
    {
        $user->setAttributes([
            'email'    => $this->email,
            'username' => $this->username,
            'password' => $this->password,
        ]);
        /** @var Profile $profile */
        $profile = \Yii::createObject(Profile::className());
        $profile->setAttributes([
            'fullname' => $this->fullname,
            'sex' => $this->sex,
            'location' => $this->location,
        ]);
        $user->setProfile($profile);
    }

}

<?php

use yii\db\Migration;

/**
 * Class m201225_090045_user_module_init
 */
class m201225_090045_user_module_init extends Migration
{
    public function up()
    {
        Yii::$app->runAction('migrate/up', [
            'migrationPath' => '@vendor/dektrium/yii2-user/migrations',
            'interactive' => false
        ]);
    }

    public function down()
    {
        echo "m201225_090045_user_module_init cannot be reverted.\n";

        return false;
    }
}

<?php

use yii\db\Migration;

/**
 * Class m201225_090559_rbac_init
 */
class m201225_090559_rbac_init extends Migration
{
    public function up()
    {
        Yii::$app->runAction('migrate/up', [
            'migrationPath' => '@yii/rbac/migrations/',
            'interactive' => false
        ]);
    }

    public function down()
    {
        echo "m201225_090559_rbac_init cannot be reverted.\n";

        return false;
    }
}

<?php

use yii\db\Migration;

/**
 * Class m201225_091625_create_rbac_role
 */
class m201225_091625_create_rbac_role extends Migration
{
    public function up()
    {
        $auth = Yii::$app->authManager;

        $admin = $auth->createRole('admin');
        $admin->description = 'Admin';
        $auth->add($admin);
    }

    public function down()
    {
        $auth = Yii::$app->authManager;

        $admin = $auth->getRole('admin');
        $auth->remove($admin);
    }

}

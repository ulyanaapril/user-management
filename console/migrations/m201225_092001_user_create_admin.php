<?php

use dektrium\user\models\User;
use yii\db\Migration;

/**
 * Class m201225_092001_user_create_admin
 */
class m201225_092001_user_create_admin extends Migration
{
    public function up()
    {
        Yii::$app->runAction('user/create', [
            'admin@example.com',
            'admin',
            '123456'
        ]);
        $auth = Yii::$app->authManager;
        $adminRole = $auth->getRole('admin');
        $auth->assign($adminRole, User::findOne(['username' => 'admin'])->id);
    }

    public function down()
    {
        $user = User::findOne(['username' => 'admin']);
        Yii::$app->authManager->revokeAll($user->id);
        Yii::$app->runAction('user/delete', [
            'admin',
            'interactive' => false
        ]);
    }
}

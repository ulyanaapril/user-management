<?php

use yii\db\Migration;

/**
 * Class m201225_142846_add_fields_to_profile_table
 */
class m201225_142846_add_fields_to_profile_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('profile', 'sex', $this->string(50)->null());
        $this->addColumn('profile', 'fullname',   $this->string(150)->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('profile', 'sex');
        $this->dropColumn('profile', 'fullname');
    }

}

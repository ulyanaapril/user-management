<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%invite}}`.
 */
class m201227_115158_create_invite_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%invite}}', [
            'id' => $this->primaryKey(),
            'username' => $this->string(255),
            'email' => $this->string(100),
            'sent_date' => $this->string(100)->null(),
            'registration_date' => $this->string(100)->null(),
            'last_signin' => $this->string(100)->null(),
            'sex' => $this->string(50),
            'location' => $this->string(150),
            'status' => $this->string(50),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%invite}}');
    }
}

<?php

use yii\db\Migration;

/**
 * Class m201227_141938_add_field_to_invite_table
 */
class m201227_141938_add_field_to_invite_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('invite', 'token', $this->string());
        $this->addColumn('invite', 'user_id', $this->integer(11)->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('invite', 'token');
        $this->dropColumn('invite', 'user_id');
    }
}

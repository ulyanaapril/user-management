<?php

namespace frontend\controllers;

use common\models\Profile;
use dektrium\user\models\User;
use Yii;
use common\models\Invite;
use common\models\InviteSearch;
use yii\db\ActiveRecord;
use yii\filters\AccessControl;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use function Sodium\randombytes_buf;

/**
 * InviteController implements the CRUD actions for Invite model.
 */
class InviteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'update', 'delete', 'view'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['approve'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Invite models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new InviteSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Invite model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * @return string|\yii\web\Response
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\Exception
     */
    public function actionCreate()
    {
        $model = new Invite();

        $fields = Yii::$app->request->post('Invite');

        $transaction = ActiveRecord::getDb()->beginTransaction();

        if ($model->load(Yii::$app->request->post())) {
            $model->token = rand(4, 32) . time();
            $model->sent_date = time();
            if ($model->save()) {
                /** @var User $user */
                $user = \Yii::createObject([
                    'class' => User::className(),
                    'scenario' => 'create',
                ]);

                $user->username = $fields['username'];
                $user->email = $fields['email'];

                if ($user->validate() && $user->create()) {
                    $profile = Profile::find()->where(['user_id' => $user->id])->one();
                    $profile->location = $fields['location'];
                    $profile->sex = $fields['sex'];
                    $profile->fullname = $fields['username'];
                    if ($profile->save()) {
                        $model->user_id = $user->id;
                        $model->save(false);
                        $link = Html::a('site', Url::to(['invite/approve', 'user_id' => $user->id, 'token' => $model->token], true));
                        Yii::$app->mailer->compose()
                            ->setFrom(Yii::$app->user->identity->email)
                            ->setTo($user->email)
                            ->setSubject('Invite')
                            ->setTextBody('Plain text content')
                            ->setHtmlBody("<b>You are invited to authentication. Go to {$link}</b>")
                            ->send();
                        $transaction->commit();
                        \Yii::$app->getSession()->setFlash('success', \Yii::t('user', 'User has been created'));
                        return $this->redirect(['update', 'id' => $model->id]);
                    } else {
                        $transaction->rollBack();
                    }
                } else {
                    $transaction->rollBack();
                }
            }
        } else {
            $transaction->rollBack();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * @param $user_id
     * @param $token
     * todo new password form after redirect
     */
    public function actionApprove($user_id, $token) {
        $invite = Invite::find()->where(['token' => $token])->andWhere(['user_id' => $user_id])->one();
        if (!empty($invite)) {
            $user = User::find()->where(['id' => $user_id])->one();
            if (!empty($user)) {
                return $this->redirect(['user/login']);
            }
        }
        \Yii::$app->getSession()->setFlash('error', \Yii::t('user', 'Wrong link'));
        return $this->redirect(['user/login']);

    }

    /**
     * Updates an existing Invite model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Invite model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Invite model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Invite the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Invite::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
